import java.lang.reflect.Array;
import java.util.*;

public class AliceandBob {
	
	public  static int findMaxdiff(int[] arr1, int[][] arr2) {
		
		
		
		//Arrays.sort(arr2,(a,b)->Integer.compare(a[0], b[0]));
		
		
	
		int sum2=0,sum1=0;
		int pref_arr[] = new int[arr1.length+1];
		for(int i=0;i<arr2.length;i++) {
			pref_arr[arr2[i][0]-1]+=1;
			pref_arr[arr2[i][1]]-=1;
		}
		
		for(int i=1;i<arr1.length;i++) {
			pref_arr[i]+=pref_arr[i-1];
		}
		
		for(int j=0;j<arr1.length;j++)
		sum2+=pref_arr[j]*arr1[j];
	
		
		
		Arrays.sort(arr1);
		Arrays.sort(pref_arr);
		
		int len=pref_arr.length;
		for(int i=arr1.length-1;i>=0;i--) {
		sum1+=arr1[i]*pref_arr[len-1];
		len--;
		}
		
		
		
		
		
		
	
		return sum1-sum2;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		
		int test=sc.nextInt();
		for(int t=0;t<test;t++) {
			int n =sc.nextInt();
			int k=sc.nextInt();
			
			int arr1[] =new int [n];
			
			
			int arr2[][] = new int[k][2]; 
			for(int i=0;i<n;i++) {
				arr1[i]=sc.nextInt();
			}
			
			for(int i=0;i<k;i++) {
				arr2[i][0]=sc.nextInt();
				arr2[i][1]=sc.nextInt();
			}
			
			
			int ans=findMaxdiff(arr1,arr2);
			
			System.out.println(ans+" ");
			
		}
		
		

	}

	

}
