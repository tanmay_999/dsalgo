import java.util.*;

class TAN{ 
  
    // Function to return the 
    // largest number possible 
    static long maxnumber(long n, int k) 
    { 
        // Generate the largest number 
        // after removal of the least 
        // K digits one by one 
        for (int j = 0; j < k; j++) { 
  
            long ans = 0; 
            int i = 1; 
  
            // Remove the least digit 
            // after every iteration 
            while (n / i > 0) { 
  
                // Store the numbers formed after 
                // removing every digit once 
                long temp = (n / (i * 10)) 
                               * i 
                           + (n % i); 
                i *= 10; 
  
                // Compare and store the maximum 
                ans = Math.max(ans, temp); 
            } 
            n = ans; 
        } 
  
        // Return the remaining number 
        // after K removals 
     //   return (long) (n %(Math.pow(10, 9)+7)); 
        return n;
        
    } 
  
    // Driver code 
    public static void main(String[] args) 
    { 
    	Scanner sc = new Scanner(System.in);
    	long n= sc.nextLong();
        
        int k = 1; 
  
        System.out.println(maxnumber(n, k)); 
    } 
} 